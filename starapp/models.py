from django.db import models

# Create your models here.

class Project(models.Model):
    title_en=models.CharField(max_length=20)
    title_ar=models.CharField(max_length=20)
    description_en=models.TextField(max_length=255)
    description_ar=models.TextField(max_length=255)
    main_image=models.ImageField(upload_to='projects_main/%y/%m/%d',null=True,blank=True)
    


    class Meta:
        verbose_name_plural='Projects'
    
        
    def __str__(self):
        return self.title_en


class Property(models.Model):
    project=models.ForeignKey(Project,on_delete=models.PROTECT)
    title_en=models.CharField(max_length=20)
    title_ar=models.CharField(max_length=20)
    description_en=models.TextField(max_length=255)
    description_ar=models.TextField(max_length=255)
    main_image=models.ImageField(upload_to='properties_main/%y/%m/%d',null=True,blank=True)
    cover_image=models.ImageField(upload_to='properties_cover/%y/%m/%d',null=True,blank=True)
    area=models.PositiveSmallIntegerField()
    livingrooms=models.PositiveSmallIntegerField()
    bedrooms=models.PositiveSmallIntegerField()
    bathrooms=models.PositiveSmallIntegerField()
    kitchens=models.PositiveSmallIntegerField()
    garages=models.PositiveSmallIntegerField()

    class Meta:
        verbose_name_plural='Properties'
    
        
    def __str__(self):
        return self.title_en



class ProjectMedia(models.Model):
    project=models.ForeignKey(Project,related_name='project_media',on_delete=models.PROTECT)
    image=models.ImageField(upload_to='projectmedia/%y/%m/%d',null=True,blank=True)
    is_3d=models.BooleanField(default=True)

    class Meta:
        verbose_name_plural='Project Media'
        #ordering=('-created',)

 



class PropertyMedia(models.Model):
    property=models.ForeignKey(Property,related_name='property_media',on_delete=models.PROTECT)
    image=models.ImageField(upload_to='propertiesmedia/%y/%m/%d',null=True,blank=True)
    is_3d=models.BooleanField(default=True)

    class Meta:
        verbose_name_plural='Property Media'
    
        





class Reviews(models.Model):
    RATING_RANGE = (
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5')
    )

    name_en=models.CharField(max_length=20)
    name_ar=models.CharField(max_length=20)
    position_en=models.CharField(max_length=30)
    position_ar=models.CharField(max_length=30)
    message_en=models.TextField(max_length=255)
    message_ar=models.TextField(max_length=255)
    image=models.ImageField(upload_to='reviews/%y/%m/%d',null=True,blank=True)
    rate=models.CharField(max_length=10,choices=RATING_RANGE,)
    


    class Meta:
        verbose_name_plural='Reviews'
    
        
    def __str__(self):
        return self.name_en