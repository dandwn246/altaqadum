from rest_framework import serializers
from .models import *



class PropertiesMediaSerializers(serializers.ModelSerializer):
    class Meta:
        model=PropertyMedia
        fields='__all__' 


class PropertiesSerializers(serializers.ModelSerializer):
    property_media=PropertiesMediaSerializers(many=True,read_only=True)
    class Meta:
        model=Property
        fields='__all__'     
        extra_fields=['property_media']
           


class ProjectsMediaSerializers(serializers.ModelSerializer):
    #project=Projects.objects.all()
    class Meta:
        model=ProjectMedia
        fields='__all__'         
       


class ProjectsSerializers(serializers.ModelSerializer):
    project_media=ProjectsMediaSerializers(many=True,read_only=True)

    class Meta:
        model=Project
        fields='__all__'
        extra_fields=['project_media']         
           


class ReviewsSerializers(serializers.ModelSerializer):
    class Meta:
        model=Reviews
        fields='__all__' 


class ContactSerailizer(serializers.Serializer):
    name = serializers.CharField()
    email = serializers.EmailField()
    message = serializers.CharField()        