from django.urls import path,include
from .views import *

 
app_name='starapp'

urlpatterns = [
    path('getproject',get_project,name='get_project'),
    path('getproperty/<int:id>',get_property,name='get_property'),
    path('reviews',get_reviews,name='get_review'),
    path('contact',api_create_contact_view,name='contact_us'),
   
    
    

]