

# Create your views here.
from django.shortcuts import render
from .models import *
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .serializers import *
from django.core.mail import send_mail
from rest_framework import status
# Create your views here.



@api_view(['GET'])
def get_project(request):
    if request.method=='GET':
        project=Project.objects.all()
        print(project)
        data=ProjectsSerializers(project,many=True).data
        return Response({'data':data})


@api_view(['GET'])
def get_property(request,id):
    if request.method=='GET':
        property=Property.objects.filter(project=id)
        data=PropertiesSerializers(property,many=True).data
        return Response({'data':data})        


@api_view(['GET'])
def get_reviews(request):
    if request.method=='GET':
        reviews=Reviews.objects.all()
        data=ReviewsSerializers(reviews,many=True).data
        return Response({'data':data})



@api_view(['POST',])
def api_create_contact_view(request):
    if request.method == "POST":
        serializer = ContactSerailizer(data=request.data)
        if serializer.is_valid():
            name = serializer.data["name"]
            email = serializer.data["email"]
            message = serializer.data["message"]

            # send mail
            send_mail(
                'Contact Form mail from ' + name,
                message,
                email,
                ['altaqadum6@gmail.com'],
            )
            
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)         