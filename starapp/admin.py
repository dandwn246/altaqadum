from django.contrib import admin

# Register your models here.
from.models import *

# Register your models here.
@admin.register(Project)
class ProjectsAuthorAdmin(admin.ModelAdmin):
    list_display=['title_en','title_ar','id','main_image']
    list_display_links=['title_en','title_ar','id','main_image']
    search_fields=['title_en','title_ar','id']



@admin.register(Property)
class PropertiesAuthorAdmin(admin.ModelAdmin):
    list_display=['title_en','title_ar','id','main_image','cover_image','project','area']
    list_display_links=['title_en','title_ar','id','main_image','cover_image','project','area']
    search_fields=['title_en','title_ar','id']    


@admin.register(ProjectMedia)
class ProjectsMediaAuthorAdmin(admin.ModelAdmin):
    list_display=['id','image','project','is_3d']
    list_display_links=['id','image']
    search_fields=['id']    


@admin.register(PropertyMedia)
class PropertiesMediaAuthorAdmin(admin.ModelAdmin):
    list_display=['id','image','property','is_3d']
    list_display_links=['id','image']
    search_fields=['id','property']     


@admin.register(Reviews)
class ReviewsAuthorAdmin(admin.ModelAdmin):
    list_display=['name_en','name_ar','id','position_en','position_ar','image','message_en','message_ar','rate']
    list_display_links=['name_en','name_ar','id','position_en','position_ar','image','message_en','message_ar',]
    search_fields=['name_en','name_ar','id'] 